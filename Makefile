APP_NAME="eros" #TODO parse from `Cargo.toml`
PORT_OPT="-p8080:8080"
TEST_YML="docker-compose.dev.yml"

up:
	docker-compose up

down:
	docker-compose down

ps:
	docker-compose ps

run:
	docker-compose run -T --entrypoint "/home/aporia/app/de run" $(PORT_OPT) bin

test:
	docker-compose run -T --entrypoint "/home/aporia/app/de test" $(PORT_OPT) bin

check:
	docker-compose run -T --entrypoint "/home/aporia/app/de check" $(PORT_OPT) bin

cargo-build:
	docker-compose up -f docker-compose.yml -f docker-compose.dev.yml


stop:
	docker stop $(APP_NAME) && \
	docker rm $(APP_NAME)
