FROM rust:1.26-slim as builder
# NB: build from an *existing* cargo project--that is, have already run, e.g.,
# ``cargo init --bin`.

# make home dir; set shell=bash;
RUN useradd -ms /bin/bash aporia
USER aporia

RUN mkdir -p /home/aporia

ENV CARGO_HOME=/home/aporia/.cargo

# cache deps
RUN cd /home/aporia/ && \
    export USER=aporia && \
    cargo new --bin app

WORKDIR /home/aporia/app

RUN touch ./src/lib.rs

# run dummy build to cache deps
COPY Cargo.toml /home/aporia/app/Cargo.toml
COPY Cargo.lock /home/aporia/app/Cargo.lock
#COPY src/lib.rs /home/aporia/app/src/lib.rs

# clean up dep cache mess
RUN cargo build --release
RUN rm /home/aporia/app/**/*.rs

COPY ./src /home/aporia/app/src
#COPY /target/ /home/aporia/app/target/

# TODO: `docker-compose up` did _NOT_ work when this was omitted.

USER root
RUN chown -R aporia /home/aporia
USER aporia


RUN cargo build
COPY ./de /home/aporia/app/de
FROM rust:1.26-slim
COPY --from=builder /home/aporia/app .
WORKDIR /home/aporia/app

CMD ["cargo", "check"]
