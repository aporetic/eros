Refactor `eros` such that the following graph (in psuedo-code):
    
    ```rust
    let g = Graph { 
        nodes: [a, b, c, Slice(a)],
        edges: [(a,b), (c, Slice(a)]
    }
    ```

=======
### Summary

Replace the untyped directed edges of previous versions with weighted, typed
edges . In theory, the weights ought to by fully polymorphic, but, edge type
dependent dispatch--e.g., graph rendering, slice viewing, etc.--_must_ be
statically defined, and therefore so must the edge enumeration.

### Example 
Refactor eros such that the following graph (in psuedo-code):
```rust
let g = Graph { 
    nodes: [a, b, c, Slice(a)],
    edges: [(a,b), (c, Slice(a)]
}
```
which fails, at least when rendered, to cleary denote the relationship between
`a` and `Slice(a)`--that is, which does not indicate that `Slice(a)` depends
upon `a` in order to exist meaningfully--, is modeled, instead, like the
following:
    
    ```rust
    let h = Graph {
        nodes: [a, b, c],
        edges: [ (All(a), All(b)),
                 (All(c), Partial(a, Sel(..)))]
    }
    ```
where, an edge takes the form `(NodeSel, NodeSel)`, 
and where `NodeSel` is one of `All(NodeId) | Partial(Nodeid, Sel)`.

Alternatively, an edge could take the form 
`(src_sel: Sel, src: NodeId, tgt_sel: Sel, tgt: NodeId)` 


A diagram of the above. This:

    All(a) -> Part(b),
    All(c) -> Part(a),

becomes:
 
    b -All,All-->  a,
    c -All,Part--> a // viz. _all_ of c, selects _part_ of a


NB: All above references to nodes and edges apply _only_ to "full", or
"absolute" edges; that is, edges and nodes modeled according to the common
mathematical definiton of a graph as consisting in a tuple of a vector of edges
$`\vec{e}`$ and nodes $`\vec{n}`$, given a vector of nodes $`\vec{n}`$, and of edges
$`\vec{e}`$, then there exists some graph $`g = (\vec{n},\vec{e})`$.


### Implementation details
#### Out with the old, but not before a review
Let's start by inspecting a selection of GraphQL query fields.
    
Here are crucial fields of `QueryRoot`:
```haskell
-- Fetches node by id.
node :: String -> Node 

-- the content of the node--of type string a.t.m, but destined for greater polymorphism.
deref :: Node -> String

-- nodes to which the current node points
pointsTo :: Node -> [Node] 

# nodes pointed to by the current node
isPointedToBy :: Node -> [Node] 
```

#### In with the new
First, let us (informally) hash out the the skeleton of the proposed
[GraphQL][gql] API revision.

```haskell
--
-- `pointsTo` becomes `selects`
selects :: Node -> [PointsTo]

-- `isPointedToBy` becomes `isSelectedBy`
isSelectedBy :: Node -> [IsPointedToBy]
```

`PointsTo` and `IsPointedToBy` represent edges within a context that contains
a single current, focused node which is omitted from the "relative" edge. For
example, say a document is focused, and a user asks for all those nodes which
select the document; then the user will receive a list of `IsSelectedBy`, none
of which will contain the document's identifier because it is assumed to be
known within the context of the request.


[gql]: https://graphql.org

/label ~feature-request
/cc @project-manager
