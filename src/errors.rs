// errors.rs

use super::r2d2;

error_chain!{
    foreign_links {
        Fmt(::std::fmt::Error);
        Utf8(::std::str::Utf8Error);
        IO(::std::io::Error);
        Redis(::redis::RedisError);
        Serde(::serde_json::error::Error);
        R2D2(r2d2::Error);
        R2D2Redis(::r2d2_redis::Error);
        //Juniper(::juniper::FieldError);
    }
}
