// graphql.rs

use serde;
use serde_json;

use errors::*;
use juniper::{FieldError, FieldResult, InputValue, RootNode, Value};
use vmap::DB;
use vmap::atom::{Atom, AtomBody, AtomId, AtomSlice, AtomView, Selection};
use vmap::id::Prefix;
use vmap::node::{PartialNode, FullNode, NewNode, Node, NodeId};
use vmap::edge::{Weight, Edge};

graphql_object!(Atom: DB |&self| {
    description: "Atom of eros; contains id, body."

    field id() -> AtomView {
        (&self.id).to_owned()
    }

    // since `AtomBody` is a `GraphQLInputObject`, it cannot be returned from a
    // field? why?
    // perhaps:
    // * returning non `GraphQLType` from field ?
    field body() -> AtomView
    as "Fetch body of `Atom` as gql obj." {
        (&self.body).to_owned()
    }

    field deref() -> AtomView
        // TODO handle atomId enum
    as "View body of atom, i.e., its content." {
        self.body.to_owned()
    }
});

graphql_object!(AtomBody: DB |&self| {
    description: "The content of an `Atom`."
    field deref() -> String {
        // TODO handle atomId enum
        self.body().to_owned()
    }

    field len() -> i32 {
        self.len() as i32
    }

});

graphql_object!(AtomId: DB |&self| {
    description: "An `AtomId`--this may become opaque in the future."

    field id() -> String as "Fetch AtomId subject to slice's selection" {
        self.id().to_owned()
    }

    field deref(&executor) -> FieldResult<AtomView> as "Dereference `AtomId`, return `AtomView`." {
        // TODO handle atomId enum
        let db = executor.context();
        let s: String = db.getj(self.id(), ".")?; // slice | body | node -- as json
        let prefix =  Prefix::from_str(self.id())?;
        println!("AtomId: field deref: s = {:#?}, prefix: {:?}", &prefix, s);

        match prefix {
            Prefix::BodyId => {
                let body = serde_json::from_str::<AtomBody>(&s)?;
                Ok(body.body().to_owned())
            },
            Prefix::SliceId => {
                let slice = serde_json::from_str::<AtomSlice>(&s)?;
                let s = db.getj(slice.atom_id_as_str(), ".")?;
                let body: AtomBody = serde_json::from_str(&s)?;
                Ok(body.body().to_owned())
            },
            Prefix::NodeId => Err( "AtomId: (graphQL) field `deref` \
                found `Prefix::NodeId`, expected one of: `Prefix::{Body,Slice}Id`.".into()),
        }

    }
});

// juniper integration
graphql_object!(Node: DB as "Node" |&self| {
description: "A Node in the content graph."

// NB: use `fn ... -> T as "<description>" to queryable type descriptions (in
// place of doc comments).
field id() -> &str as "unique `NodeId`" { &self.id }
field atom_id() -> AtomId as "content `AtomId`" { self.content.clone() }
// use this field when references should be followed. This returns the
// referent. if references are the desired output see `points_to_ids`.

field deref(&executor) -> FieldResult<AtomView> as "Deref `Node`s `AtomId` and display the content." {
        // TODO handle atomId enum
    let db = executor.context();
    // lookup `AtomId`.
    //let atom: AtomBody = db.get(self.content.clone())?;
    let s: String = db.getj(self.content.id(), ".")?;
    // TODO `AtomId` prefixes specifying variant, i.e., `AtomBody`,
    // `AtomSlice`.
    // TODO fix the ugly mess below
    let prefix =  Prefix::from_str(self.content.id())?;
    println!("Node::deref: {:#?}, prefix = {:?}", &prefix, s);
    match prefix {
            Prefix::BodyId => {
                let body = serde_json::from_str::<AtomBody>(&s)?;
                Ok(body.body().to_owned())
            },
            Prefix::SliceId => {
                let slice = serde_json::from_str::<AtomSlice>(&s)?;
                let s = db.getj(slice.atom_id_as_str(), ".")?;
                let body: AtomBody = serde_json::from_str(&s)?;
                Ok(body.body().to_owned())
            },
            Prefix::NodeId => Err("`Node` field `deref`: could not convert json \
                 into `AtomBody` or `AtomSlice`.".into()),
    }
}

field points_to_ids() -> Vec<NodeId> as "List of nodes to which the parent points." {
    println!("points_to_ids: {:#?}", &self);
    self.points_to.clone()
}

field is_pointed_to_by_ids() -> Vec<NodeId> as "List of nodes which point to the parent." {
    println!("is_pointed_to_by_ids: {:#?}", &self);
    self.is_pointed_to_by.clone()
}

field points_to(&executor) -> FieldResult<Vec<Node>> {
    let db = executor.context();
    let mut ret = vec![];
    for bid in self.points_to.iter() {
        if let Ok(ref x) = db.get_by_id(bid) {
            //todo: convert to Blob
            let nb: ::std::result::Result<NewNode, serde_json::Error> = serde_json::from_str(&x);
            if let Ok(node) = nb {
                ret.push(node.into_node(&bid));
            }
        }
    }
    //show(&ret);
    Ok(ret)
}
field is_pointed_to_by(&executor) -> FieldResult<Vec<Node>> {
    let db = executor.context();
    let mut ret = vec![];
    for bid in self.is_pointed_to_by.iter() {
        if let Ok(ref x) = db.get_by_id(bid) {
            //todo: convert to Blob
            let nb: ::std::result::Result<NewNode, serde_json::Error> = serde_json::from_str(&x);
            if let Ok(node) = nb {
                ret.push(node.into_node(&bid));
            }
        }
    }
    //show(&ret);
    Ok(ret)
}
});

graphql_object!(AtomSlice: DB |&self| {
    description: "A slice-view of some AtomBody."

    field atom() -> AtomId as "Fetch AtomId subject to slice's selection" {
        self.atom_id()
    }

    field deref(&executor) -> FieldResult<AtomView> as "Dereference `AtomId`, return `AtomView`." {
        // TODO handle atomId enum
        let db = executor.context();
        let s: String = db.getj(self.atom_id_as_str(), ".")?;
        println!("AtomSlice: field deref: s = {:#?}", s);
        let atom: AtomBody = serde_json::from_str(&s)?;
        // GRAPHIC CONTENT WARNING: graphemes get rekt on the following line
        let s = self.s();
        let e = self.e();
        Ok(atom.body().chars().skip(s).take(e - s).collect()) // flagged as bugworthy
    }
});

pub struct MutationRoot;

graphql_object!(MutationRoot: DB |&self| {
    description: "MutationRoot descr"
    field empty() -> &str { "empty" }

    // Should this return an `AtomBody` (which would necesitate my creating an
    // grapqhl object analogue for `AtomBody`) or simply an `AtomId` (meaning
    // client side query composition).
    field create_atom_from_str(&executor, s: String) -> FieldResult<AtomId> {
        let db = executor.context();
        Ok(db.set_atomic_chunk(&s)?)
    }

    // Create a node from an `AtomBody` with empyt reference lists.
    field create_node(&executor, atom_id: String)
        -> FieldResult<Node> as "Create node from `AtomId`" {
        let db = executor.context();
        let aid = AtomId::new(&atom_id);
        let id = db.new_node(&aid)?;
        Ok(db.get_node_by_id(&id.id())?)
    }

    // Link two `Node`s.
    field link(&executor, n0: NodeId, n1: NodeId) -> FieldResult<Vec<Node>>
    as "Links two `Node`s by id such that n0 points to n1, and n1 is pointed to by n0" {
        let db = executor.context();
        db.link(&n0, &n1)?;
        let node0 = db.get_node_by_id(&n0)?;
        let node1 = db.get_node_by_id(&n1)?;
        Ok(vec![node0, node1])
    }

    // select from atom. Does _NOT_ actuall insert the `AtomSlice` into the db,
    // just displays the content of it. This is a one-off op mostly for
    // testing.
    field view_selection(&executor, atom_id: String, s: i32, e: i32 ) -> AtomSlice
    as "Fetches span of selection, which `AtomSlice` is not preserved." {
        let sel = Selection::new_from_signed(s, e);
        let aid = AtomId::new(&atom_id);
        AtomSlice::new(aid, sel)
    }

    // Select from atom, *and* insert into db, return `AtomId`.
    //
    // Perhaps use to generate `Node`.
    field deprecated "`Atom`s are meant only to be selected through `Node`s."
        select_from_atom(&executor, atom_id: String, s: i32, e: i32) -> FieldResult<AtomId>
        as "Creates and inserts `AtomSlice`; returns redis key/id." {
        let db = executor.context();
        let aid = AtomId::new(&atom_id);
        let id_res = db.set_atom_slice(aid, s, e);
        id_res.map_err(to_field_error!("serde_json: failed to convert redis val to `AtomId`"))
    }

    // Select from `Node`. Use this over `select_from_atom`.
    //field select(&executor, nodeId: String, s: i32, e: i32) -> FieldResult<Node> {
    //    //let db = executor.context();
    //    db.set_atom_slice(
    //}
});

pub struct QueryRoot;

graphql_object!(QueryRoot: DB |&self| {
    description: "query root blob"

    field node(&executor, id: NodeId) -> FieldResult<Node> as "Query node by (locally) unique `NodeId`." {
        let db = executor.context();
        db.getj(&id, ".")
          .and_then(|ref s| {
              let nn: NewNode = serde_json::from_str(s)?;
              Ok(nn.into_node(&id)) })
          .map_err(to_field_error!("serde_json: failed to parse `&str` to `NewNode`."))
    }

    field atom(&executor, id: String) -> FieldResult<Atom> as "Fetch `Atom` by unique `AtomId` string." {
        let db = executor.context();
        db.getj(&id, ".")
          .and_then(|ref s| {
              let body: AtomBody = serde_json::from_str(s)?;
              let aid = AtomId::new(&id);
              let atom = Atom::new(body, aid);
              Ok(atom)


          })
          .map_err(to_field_error!("serde_json: failed to parse `String` to `AtomBody`."))
    }

    // NB: fields _cannot_ return `AtomBody`.
    // TODO have this return `Vec<FullAtom>`
    field atoms(&executor) -> FieldResult<Vec<Atom>> as "Fetch all `Atom`s in db." {
        let db = executor.context();
        let keys: Vec<String> = db.keys("*")?;
        let mut atoms: Vec<Atom> = vec![];
        for k in keys.iter() {
            let s: String = db.type_of(k)?;
            //println!("nodes: type_of key = {:?}", s);
            if &s == "ReJSON-RL"{
                let n: String = db.getj(k, ".")?;
                let r: ::std::result::Result<AtomBody, serde_json::Error>
                    = serde_json::from_str(&n);
                if let Ok(body) = r {
                    let id = AtomId::new(k); // id, body match NOT checked
                    atoms.push(Atom::new(body, id));
                }
            }
        }
        Ok(atoms)
    }

    field nodes(&executor) -> FieldResult<Vec<Node>> as "Fetch all `Node`s in db." {
        let db = executor.context();
        let keys: Vec<String> = db.keys("*")?;
        let mut nodes = vec![];
        for k in keys.iter() {
            let s: String = db.type_of(k)?;
            //println!("nodes: type_of key = {:?}", s);
            if s == "ReJSON-RL".to_owned() {
                let n: String = db.getj(k, ".")?;
                //println!("nodes: inside type checker: type = {:?}, val as str = {:?}.", &s, &n);
                let node: ::std::result::Result< NewNode, serde_json::Error>
                        = serde_json::from_str(&n);
                if let Ok(n) = node {
                    //println!("successfully parsed from json");
                    nodes.push(n.into_node(k));
                }
            }
        }
        Ok(nodes)
    }
});

/// GraphQL query root. Everything begins here.
pub type Schema = RootNode<'static, QueryRoot, MutationRoot>;

pub fn create_schema() -> Schema {
    Schema::new(QueryRoot {}, MutationRoot {})
} 

// essay # 1: w.r.t. edge variants
graphql_object!(FullNode<NodeId, Weight>: DB as "FullNode" |&self| {
    field  id() -> &str as "node identifier" { self.id() }
    // edge-variant dependent behavior dispatched elsewhere, this just returns
    // the `AtomId` unlike the last gql schema
    field atom_id() -> AtomId { self.atom_id() }
    // TODO make it so that a) fields can return `RelativeEdge`s, and b) so a
    // `Vec<PointsTo>` could point to nodes with edges _other_ than `Select`.
    // NB; for now this will return `AtomId` as a PLACEHOLDER
    field selects_ids() -> Vec<NodeId> 
        as "Fetches `NodeId`s upon which the current node comments" {
        self.selects().into_iter().map(|p| {
            p.node()
        }).collect()
    }
    

});

