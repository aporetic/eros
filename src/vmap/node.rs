// node.rs
//! Provides structs `Node`, `NewNode`, etc..

use std::any::Any;

use serde;
use serde_derive;

use vmap::atom::AtomId;
use vmap::edge::{Direction, Edge, EdgeType, GenericDirRelEdge, PointsTo, Sel, Select,
                 WeightedRelEdge, IsPointedToBy};

pub type NodeId = String;

/// (NEW) Virtual graphql object.
pub struct FullNode<N, W> {
    id: N,
    body: AtomId,
    adj: Vec<Edge<N, W>>,
}

impl<N: Clone, W> FullNode<N, W> {
    pub fn id(&self) -> &N {
        &self.id
    }
    
    pub fn new(id: N, body: AtomId, adj: Vec<Edge<N, W>>) -> Self {
        FullNode { id, body, adj }
    }

    pub fn atom_id(&self) -> AtomId {
        self.body.to_owned()
    }

    pub fn selects(&self) -> Vec<PointsTo<N>> {
        let mut ret = Vec::new();
        for edge in self.adj.iter() {
            if let Edge::Select(sel) = edge {
                if let Some(e) = PointsTo::try_from(sel) {
                    ret.push(e)
                }
            }
        }
        ret
    }

    pub fn is_selected_by(&self) -> Vec<IsPointedToBy<N>> {
        let mut ret = Vec::new();
        for edge in self.adj.iter() {
            if let Edge::Select(sel) = edge {
                if let Some(e) = IsPointedToBy::try_from(sel) {
                    ret.push(e)
                }
            }
        }
        ret
    }
}

/// (NEW) Will replace `Node`.
pub struct PartialNode<N, W> {
    body: AtomId,
    adj: Vec<Edge<N, W>>,
}

impl<N: Clone, W> PartialNode<N, W> {
    pub fn new(body: AtomId, adj: Vec<Edge<N, W>>) -> Self {
        PartialNode { body, adj }
    }

    pub fn selects(&self) -> Vec<PointsTo<N>> {
        let mut ret = Vec::new();
        for edge in self.adj.iter() {
            if let Edge::Select(e) = edge {
                if let Some(isptb) = PointsTo::try_from(e) {
                    ret.push(isptb);
                }
            }
        }
        ret
    }

    pub fn is_selected_by(&self) -> Vec<IsPointedToBy<N>> {
        let mut ret = Vec::new();
        for edge in self.adj.iter() {
            if let Edge::Select(e) = edge {
                if let Some(isptb) = IsPointedToBy::try_from(e) {
                    ret.push(isptb);
                }
            }
        }
        ret
    }

    pub fn from_atom_id(atom_id: &AtomId) -> Self {
        PartialNode {
            body: atom_id.clone(),
            adj: vec![],
        }
    }

    fn into_full_node(self, id: &N) -> FullNode<N, W> { 
        FullNode {
            id: id.to_owned(),
            body: self.body,
            adj: self.adj
        }
    }
}

fn dir_to<T>(t: T) -> Direction<T> {
    Direction::To(t)
}

fn dir_from<T>(t: T) -> Direction<T> {
    Direction::From(t)
}

#[cfg(test)]
mod tests {
    use super::*;
    use vmap::edge::WeightedRelEdge;
    #[test]
    fn node_test() {
        let body = AtomId::new("blah blah blah"); // atom id
        let s0 = Edge::Select(Select::new(Sel::All, (Sel::Nothing, Direction::To("nid 0".to_owned()))));
        let s1 = Edge::Select(Select::new(Sel::All, (Sel::All, Direction::To("nid 1".to_owned()))));
        let s2 = Edge::Select(Select::new(Sel::All, (Sel::Nothing, Direction::To("nid 2".to_owned()))));

        let f0 = Edge::Select(Select::new(Sel::All, (Sel::Nothing, Direction::From("fnid 0".to_owned()))));
        let f1 = Edge::Select(Select::new(Sel::All, (Sel::Nothing, Direction::From("fnid 1".to_owned()))));

        let adj = vec![
            Edge::Other("json of user defined type 0".to_owned()),
            s0.clone(),
            s1.clone(),
            f0.clone(),
            f1.clone(),
            Edge::Other("json of user defined type 1".to_owned()),
            Edge::Weighted(WeightedRelEdge::new(
                dir_from("weigted nid 0".to_owned()),
                (Sel::All, Sel::All),
            )),
            s2.clone()
        ];
        let pn = PartialNode::new(body.clone(), adj.clone());
        let full = FullNode::new("id".to_owned(), body.clone(), adj.clone());
        println!("{:?}", pn.selects());

        let exp: Option<Vec<PointsTo<String>>> = vec![s0, s1, s2].into_iter().map(|e| {
            match e {
                Edge::Select(ref sel) => PointsTo::try_from(sel),
                _ => None
            }
        }).collect();

        let exp_from: Option<Vec<IsPointedToBy<String>>> = vec![f0, f1].into_iter().map(|e| {
            match e {
                Edge::Select(ref sel) => IsPointedToBy::try_from(sel),
                _ => None
            }
        }).collect();

        assert_eq!(exp.clone().unwrap(), pn.selects());
        assert_eq!(exp.unwrap(), full.selects());
        assert_eq!(exp_from.unwrap(), FullNode::new("id".to_owned(), body, adj).is_selected_by());
    }
}

#[derive(Clone, Debug, PartialEq, Eq, Serialize, Deserialize)]
pub struct Node {
    pub id: NodeId,
    pub content: AtomId,
    pub points_to: Vec<NodeId>,
    pub is_pointed_to_by: Vec<NodeId>,
}

impl Node {
    pub fn new(
        id: NodeId,
        content: AtomId,
        points_to: Vec<NodeId>,
        is_pointed_to_by: Vec<NodeId>,
    ) -> Self {
        Node {
            id,
            content,
            points_to,
            is_pointed_to_by,
        }
    }
}

#[derive(Clone, Debug, PartialEq, Eq, Serialize, Deserialize)]
pub struct NewNode {
    pub content: AtomId,
    pub points_to: Vec<NodeId>,
    pub is_pointed_to_by: Vec<NodeId>,
}

impl NewNode {
    pub fn from_atom_id(atom_id: &AtomId) -> Self {
        NewNode {
            content: atom_id.clone(),
            points_to: vec![],
            is_pointed_to_by: vec![],
        }
    }

    pub fn into_node(self, node_id: &NodeId) -> Node {
        Node::new(
            node_id.to_owned(),
            self.content,
            self.points_to,
            self.is_pointed_to_by,
        )
    }
}
