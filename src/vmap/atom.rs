// atom.rs
//! Provides `Atom`, `AtomBody`, `AtomId`, `AtomSlice`, `AtomView`, `Selection`.

use base64::{self, decode, encode};
use crypto::{digest::Digest, sha2::Sha256};
use errors::*;
use macros;
use redis::{self, FromRedisValue, ToRedisArgs};
use serde;
use serde_derive;
use serde_json;

use vmap::DB;
use vmap::id::{IdPrefix, Prefix};

pub type AtomView = String;

// TODO write accessors, make fields private
#[derive(Clone, Debug, PartialEq, Eq, Serialize, Deserialize)]
pub struct Atom {
    pub body: String,
    pub id: String,
}

impl Atom {
    /// WARNING: no verification is performed that `id` is the the correct id of
    /// `body`.
    pub fn new(body: AtomBody, id: AtomId) -> Self {
        let b = body.body().to_owned();
        let i = id.id().to_owned();
        Atom { body: b, id: i }
    }
}

// atom id, see ./mod.rs for graphql_object! field derive.
#[derive(Clone, Debug, PartialEq, Eq, Serialize, Deserialize)]
pub struct AtomId {
    id: String,
}

impl AtomId {
    pub fn new(id: &str) -> Self {
        AtomId { id: id.to_owned() }
    }

    pub fn as_bytes(&self) -> &[u8] {
        self.id.as_bytes()
    }

    pub fn id(&self) -> &str {
        &self.id
    }
}

impl ToRedisArgs for AtomId {
    fn to_redis_args(&self) -> Vec<Vec<u8>> {
        vec![self.as_bytes().to_vec()]
    }
}

impl FromRedisValue for AtomId {
    fn from_redis_value(v: &redis::Value) -> redis::RedisResult<AtomId> {
        use std::str::from_utf8;
        match *v {
            redis::Value::Data(ref bytes) => Ok(AtomId::new(from_utf8(bytes)?)),
            redis::Value::Okay => fail!((
                redis::ErrorKind::IoError,
                "Found value OK, expected AtomId.",
                format!("")
            )),
            redis::Value::Status(ref val) => fail!((
                redis::ErrorKind::IoError,
                "Found status,  expected AtomId.",
                format!("Found status {:?}, expected AtomId", val)
            )),
            _ => invalid_type_error!(v, "Response type not string compatible."),
        }
    }
}
// AtomBody
#[derive(Clone, Debug, PartialEq, Eq, Serialize, Deserialize)]
pub struct AtomBody {
    body: String,
}

impl AtomBody {
    pub fn new(atom: &str) -> Self {
        AtomBody {
            body: atom.to_owned(),
        }
    }

    pub fn body(&self) -> &str {
        &self.body
    }

    pub fn len(&self) -> usize {
        self.body.len()
    }
}

impl FromRedisValue for AtomBody {
    fn from_redis_value(v: &redis::Value) -> redis::RedisResult<AtomBody> {
        use std::str::from_utf8;
        match *v {
            redis::Value::Data(ref bytes) => {
                let s = from_utf8(bytes)?;
                let atom: ::std::result::Result<AtomBody, ::std::io::Error> =
                    serde_json::from_str(&s).map_err(|e| e.into());
                Ok(atom?)
            }

            redis::Value::Okay => fail!((
                redis::ErrorKind::IoError,
                "Found value OK, expected AtomBody.",
                format!("")
            )),
            redis::Value::Status(ref val) => fail!((
                redis::ErrorKind::IoError,
                "Found status,  expected AtomBody.",
                format!("Found status {:?}, expected AtomBody", val)
            )),
            _ => invalid_type_error!(v, "Response type not string compatible."),
        }
    }
}

// AtomSlice

#[derive(Clone, Debug, PartialEq, Eq, Serialize, Deserialize)]
pub struct AtomSlice {
    atom_id: AtomId,
    sel: Selection,
}

impl AtomSlice {
    pub fn new(atom_id: AtomId, sel: Selection) -> Self {
        AtomSlice { atom_id, sel }
    }

    pub fn new_checked(db: &DB, atom_id: AtomId, sel: Selection) -> Result<Self> {
        // get atom
        let json = db.getj(atom_id.id(), ".")?;
        let atom: AtomBody = serde_json::from_str(&json)?;
        match atom.len() > sel.e as usize {
            true => Ok(AtomSlice::new(atom_id, sel)),
            false => Err("AtomSlice::new_checked: \
                          end of selection greater \
                          than length of `AtomBody`."
                .into()),
        }
    }

    pub fn atom_id_as_str(&self) -> &str {
        self.atom_id.id()
    }

    pub fn atom_id(&self) -> AtomId {
        self.atom_id.clone()
    }

    pub fn s(&self) -> usize {
        self.sel.s
    }

    pub fn e(&self) -> usize {
        self.sel.e
    }

    // this might be the source of the bug, as this seems to give each `AtomSlice`
    // the same hash as their `AtomBody`.
    //
    // WARNING: `AtomId`s of `AtomSlice`s of the same `AtomBody` with the same `Selection`
    // will collide.
    pub fn generate(&self, db: &DB) -> Result<String> {
        let s: String = db.getj(self.atom_id_as_str(), ".")?;
        let atom: AtomBody = serde_json::from_str(&s)?;

        let body = atom.body().as_bytes();
        let s_idx = transform_u32_to_array_of_u8(self.sel.s as u32); // WARNING could discard u64, u128 idxs
        let e_idx = transform_u32_to_array_of_u8(self.sel.e as u32);
        let mut h = Sha256::new();
        h.input(body);
        h.input(&s_idx);
        h.input(&e_idx);
        let mut id = encode(h.result_str().as_bytes());
        id.insert_str(0, self.prefix());
        Ok(id)
    }

    /// View slice.
    pub fn view(&self, db: &DB) -> Result<AtomView> {
        // get atomSlice json
        let s: String = db.getj(self.atom_id_as_str(), ".")?;
        //println!("view: {:#?}", &s);
        let prefix = Prefix::from_str(self.atom_id_as_str())?;
        let mut view = String::new();
        match prefix {
            Prefix::BodyId => {
                let body = serde_json::from_str::<AtomBody>(&s)?;
                view.push_str(body.body());
            }
            Prefix::SliceId => {
                let slice = serde_json::from_str::<AtomSlice>(&s)?;
                let s = db.getj(slice.atom_id_as_str(), ".")?;
                let body: AtomBody = serde_json::from_str(&s)?;
                view.push_str(body.body());
            }
            Prefix::NodeId => bail!(
                "AtomSlice::view: found `Prefix::NodeId`, \
                 expected one of: `Prefix::{Body,Slice}Id`."
            ),
        };
        // flagged as bugworthy
        Ok(view.chars()
            .skip(self.sel.s)
            .take(self.sel.e - self.sel.s)
            .collect())
    }
}

#[derive(Clone, Debug, PartialEq, Eq, Serialize, Deserialize)]
pub struct Selection {
    // TODO refactor w unsigned ints, why won't redis-rs allow them?
    pub s: usize,
    pub e: usize,
}

impl Selection {
    pub fn new(s: usize, e: usize) -> Self {
        Selection { s, e }
    }

    pub fn new_from_signed(s: i32, e: i32) -> Self {
        Selection::new(s as usize, e as usize)
    }
}

fn transform_u32_to_array_of_u8(x: u32) -> [u8; 4] {
    let b1: u8 = ((x >> 24) & 0xff) as u8;
    let b2: u8 = ((x >> 16) & 0xff) as u8;
    let b3: u8 = ((x >> 8) & 0xff) as u8;
    let b4: u8 = (x & 0xff) as u8;
    return [b1, b2, b3, b4];
}
