// id.rs
//! Provides trait `MkId`.

use base64::{self, decode, encode};
use crypto::{digest::Digest, sha2::Sha256};
use errors::*;
use juniper;

use vmap::atom::{AtomBody, AtomSlice};
use vmap::node::{NewNode, Node};

fn test() {
    let x = juniper::Value::Null;
}

pub trait GenId {
    /// Create (passibly globally unique) content identifier.
    ///
    /// Types of identifiers.
    ///     * `NodeId`, prefix: "node_id"
    ///     * `AtomBodyId`, prefix: "body_id", (perhaps instead `{Body,Slice}Id`, no?)
    ///     * `AtomSliceId`, prefix: "slice_id"
    ///
    /// a.t.m. this is a base64 encoded sip hash.
    /// in the future ids may be one of:
    ///     - sha*
    ///     - uuids
    ///     - other
    ///
    /// NB: uniqueness is not needed by all types which satisfy `GenId`, e.g.,
    /// `NewNode` which only requires local uniqueness--that is, blobs are
    /// unique only within the scope of one host; if two previously isolated
    /// users attempt to merge their graphs, `AtomId`s would be preserved
    /// (after a scan for collisions), but `BlobId`s would be namespaced,
    /// because even equivalent blobs belonging to two users are _NOT_ intended
    /// to have the same id--they might, but they're not guaranteed to. .
    /// TLDR; discovery of equivalence only matters w.r.t. `AtomId`s, not
    /// `BlobId`s.
    fn generate(&self) -> Result<String>;
}

impl GenId for NewNode {
    /// only guaranteed local uniqueness, which means no collision checking by
    /// default.
    fn generate(&self) -> Result<String> {
        let hashed = hash_str(&self.content.id());
        let mut id = encode(hashed.as_bytes());
        id.insert_str(0, self.prefix());
        Ok(id)
    }
}

impl GenId for AtomBody {
    fn generate(&self) -> Result<String> {
        let hashed = hash_str(&self.body());
        let mut id = encode(hashed.as_bytes());
        id.insert_str(0, self.prefix());
        Ok(id)
    }
}

impl GenId for AtomSlice {
    fn generate(&self) -> Result<String> {
        unimplemented!();
        // use `AtomSlice::generate()` instead.
    }
}

/// Hash str w/ sha*.
fn hash_str(s: &str) -> String {
    let mut hasher = Sha256::new();
    hasher.input_str(s);
    // get digest as hex string
    hasher.result_str()
}

/// With this trait objects stored in a `DB` instance  register identifier
/// prefixes, which are intended to reflect object type.
pub trait IdPrefix<'a> {
    fn prefix(&self) -> &'a str;
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub enum Prefix {
    BodyId,  // of `AtomBody`
    SliceId, // of `AtomSlice`
    NodeId,  // of `Node`; well, more precisely, of `NewNode`.
}

impl Prefix {
    pub fn from_str(s: &str) -> Result<Prefix> {
        println!("Prefix::from_str: {:?}", &s[0..6]);
        match &s[0..7] {
            "body_id" => Ok(Prefix::BodyId),
            "slice_i" => Ok(Prefix::SliceId), // janky, potential BUG
            "node_id" => Ok(Prefix::NodeId),
            _ => Err("AtomSlice::view: Prefix::from_str: could not \
                      convert expected to `Prefix::*`; found unkown prefix."
                .into()),
        }
    }
}

impl ToString for Prefix {
    fn to_string(&self) -> String {
        match self {
            Prefix::BodyId => "BodyId".into(),
            Prefix::SliceId => "SliceId".into(),
            Prefix::NodeId => "NodeId".into(),
        }
    }
}

// implementors
impl<'a> IdPrefix<'a> for AtomBody {
    fn prefix(&self) -> &'a str {
        "body_id"
    }
}

impl<'a> IdPrefix<'a> for AtomSlice {
    fn prefix(&self) -> &'a str {
        "slice_id"
    }
}

impl<'a> IdPrefix<'a> for NewNode {
    fn prefix(&self) -> &'a str {
        "node_id"
    }
}
