// vmap.rs
use super::crypto::digest::Digest;
use super::crypto::sha2::Sha256;
use super::macros;
use super::r2d2::{ManageConnection, Pool};
use super::r2d2_redis::RedisConnectionManager;
use super::{errors, juniper, redis, serde, serde_derive, serde_json,
            base64::{self, decode, encode}, r2d2, r2d2_redis};

use errors::*;
use juniper::{FieldError, FieldResult, InputValue, Value};
use redis::{Commands, FromRedisValue, ToRedisArgs};

use std::convert::From;
use std::default::Default;
use std::error::Error;
use std::fmt::Display;
use std::ops::Deref;
use std::ops::Drop;
use std::ops::Fn;
use std::result;
use std::string::ToString;

pub mod atom;
pub mod edge;
pub mod id;
pub mod node;

use self::atom::{Atom, AtomBody, AtomId, AtomSlice, AtomView, Selection};
use self::id::{GenId, IdPrefix, Prefix};
use self::node::{NewNode, Node, NodeId};
// use self::atom::{AtomBody, AtomSlice};

type ConMan = RedisConnectionManager;
type PooledConn = r2d2::PooledConnection<RedisConnectionManager>;

/// redis handle, assumes the module `reJSON` is installed.
pub struct DB {
    uri: String,
    pool: r2d2::Pool<ConMan>,
}

impl juniper::Context for DB {}

impl DB {
    /// generate new conn pool from redis uri.
    pub fn new(redis_uri: &str) -> Result<Self> {
        let uri = redis_uri.to_owned();
        let manager = RedisConnectionManager::new(redis_uri)?;
        let mut conn = manager.connect()?;
        if manager.has_broken(&mut conn) {
            panic!("failed to connect to redis");
        }
        let pool = r2d2::Pool::new(manager)?;
        // configure pool here via `builder()` !!
        Ok(DB { uri, pool })
    }

    pub fn get_conn(&self) -> Result<PooledConn> {
        Ok(self.pool.get()?)
    }

    /// Fetches all keys matching pattern.
    pub fn keys<RV>(&self, pat: &str) -> Result<RV>
    where
        RV: FromRedisValue,
    {
        // no threading a.t.m.
        // must `deref` or do so manually since `PooledConn` is a smart
        // pointer.
        let pconn = self.get_conn()?;
        Ok(pconn.deref().keys(pat)?)
    }

    /// USE THIS INSTEAD OF `set`!
    /// Generates id for val, inserts val, returns id.
    pub fn set_atomic_chunk(&self, s: &str) -> Result<AtomId> {
        let atom = AtomBody::new(s);
        let id = atom.generate()?;
        let json = serde_json::to_string(&atom)?;
        self.setj(&id, ".", &json)?;
        Ok(AtomId::new(&id))
    }

    /// Set json obj at `path` from `key`'s assoc'd value.
    pub fn setj(&self, key: &str, path: &str, val: &str) -> Result<()> {
        let pconn = self.get_conn()?;
        redis::cmd("JSON.SET")
            .arg(key)
            .arg(path)
            .arg(val)
            .query(pconn.deref())?;
        Ok(())
    }

    /// Create and insert new `Node` from `AtomId`.
    ///
    /// # Panics
    ///
    /// `panic!`s if either `atom_id` doesn't exist or the generated node id
    /// does already exist.
    pub fn new_node(&self, atom_id: &AtomId) -> Result<AtomId> {
        let id = atom_id.id();
        if !self.id_exists(id)? {
            let msg = format!(
                "DB::new_node: attempted to create blob from non-existent `AtomId`\n{:?}",
                id
            );
            panic!(msg)
        };
        let new_node = NewNode::from_atom_id(atom_id);
        let node_id = new_node.generate()?;
        //if self.id_exists(&node_id)? {
        //    panic!("DB::new_blob: generated blob/node id already exists")
        //}
        let new_node_json = serde_json::to_string(&new_node)?;
        self.setj(&node_id, ".", &new_node_json)?;
        Ok(AtomId::new(&node_id))
    }

    /// Get json obj at `path`.
    pub fn getj(&self, key: &str, path: &str) -> Result<String> {
        let pconn = self.get_conn()?;
        let conn = pconn.deref();
        Ok(redis::cmd("JSON.GET").arg(key).arg(path).query(conn)?)
    }

    /// Appends the given `BlobId` (as `&str`) to `id`'s 'pointsTo' field.
    /// NB: `bid` is converted to a json string, so pass a normal rust
    /// string slice and everything will be just dandy.
    ///
    /// If append op was performed, returns true.
    fn append_points_to(&self, id: &str, bid: &str) -> Result<bool> {
        let pconn = self.get_conn()?;
        let conn = pconn.deref();
        let jid = serde_json::to_string(bid)?;
        match redis::cmd("JSON.ARRINDEX")
            .arg(id)
            .arg("points_to")
            .arg(&jid)
            .query(conn)?
        {
            // unfound
            -1 => {
                redis::cmd("JSON.arrappend")
                    .arg(id)
                    .arg("points_to")
                    .arg(jid)
                    .query(self.get_conn()?.deref())?;
                Ok(true)
            }
            // found
            _ => Ok(false),
        }
    }

    /// Appends the given `bid: BlobId` (as `&str`) to `id`'s 'pointedToBy' field.
    ///
    /// TODO prevent addition of keys which are already members
    fn append_pointed_to_by(&self, id: &str, bid: &str) -> Result<bool> {
        let pconn = self.get_conn()?;
        let conn = pconn.deref();
        let jid = serde_json::to_string(bid)?;
        match redis::cmd("JSON.ARRINDEX")
            .arg(id)
            .arg("is_pointed_to_by")
            .arg(&jid)
            .query(conn)?
        {
            // unfound
            -1 => {
                redis::cmd("JSON.arrappend")
                    .arg(id)
                    .arg("is_pointed_to_by")
                    .arg(jid)
                    .query(conn)?;
                Ok(true)
            }
            // found
            _ => Ok(false),
        }
    }

    // links two ids such that `id0 -> id` (read: id0 points to id1).
    pub fn link(&self, id0: &str, id1: &str) -> Result<()> {
        self.append_points_to(id0, id1)?;
        self.append_pointed_to_by(id1, id0)?;
        Ok(())
    }

    /// Check for existence of a key in redis db.
    pub fn id_exists(&self, id: &str) -> Result<bool> {
        Ok(self.get_conn()?.deref().exists(id)?)
    }

    /// Delete all keys of the currently selected DB.
    /// WARNING: DO NOT USE. This breaks everything.
    pub fn flush_db(&self) -> Result<String> {
        let pconn = self.get_conn()?;
        let conn = pconn.deref();
        Ok(redis::cmd("FLUSHDB").query(conn)?)
    }

    /// A shortcut/alias/wrapper for `getj` from the root path "."
    /// NB: this assumes the following schema:
    /// id: {
    ///     content: String,
    ///     pointsTo: [String],
    ///     pointedToBy: [String],
    ///
    /// }
    ///
    /// Luckily, `id` a the key of the blob object we wish to retrieve, so
    /// a.t.m. this is a dumb wrapper around `Connection::getj`.
    pub fn get_by_id(&self, id: &str) -> Result<String> {
        self.getj(id, ".")
    }

    //pub fn get_atom_id(&self, node_id: &str) -> Result<AtomId> {
    //    let s = self.getj(node_id, ".")?;
    //}

    pub fn get_node_by_id(&self, id: &str) -> Result<Node> {
        let s = self.getj(id, ".")?;
        let nn: NewNode = serde_json::from_str(&s)?;
        Ok(nn.into_node(&id.to_owned()))
    }

    pub fn type_of(&self, key: &str) -> Result<String> {
        let pconn = self.get_conn()?;
        Ok(redis::cmd("TYPE").arg(key).query(pconn.deref())?)
    }

    /// Returns redis key of newly inserted `AtomSlice` (which key is a hash of
    /// the slice's span of content).
    ///
    /// _DO NOT_ manually perform these operations--USE THESE METHODS. To do so would violate
    /// (dumb) expectations of mine.
    ///
    /// TODO replace `i32` w `u32`, cuz otherwise u dumb.
    pub fn set_atom_slice(&self, id: AtomId, s: i32, e: i32) -> Result<AtomId> {
        let sel = Selection::new_from_signed(s, e);
        let slice = AtomSlice::new_checked(&self, id, sel)?;
        let redis_key = slice.generate(&self)?;
        let json = serde_json::to_string(&slice)?;
        self.setj(&redis_key, ".", &json)?;
        Ok(AtomId::new(&redis_key))
    }

    /// Fetch `AtomSlice` from redis key.
    pub fn get_atom_slice(&self, id: AtomId) -> Result<AtomSlice> {
        let json = self.getj(id.id(), ".")?;
        let slice: AtomSlice = serde_json::from_str(&json)?;
        Ok(slice)
    }

    ///// Fetch view described by the `AtomSlice` pointed to by the given
    ///// id.
    //pub fn view_atom_slice(&self, slice_id: &str) -> Result<AtomView> {
    //    let json = self.getj(slice_id, ".")?;
    //    let slice: AtomSlice = serde_json::from_str(&json)?;
    //    let atom_json = self.getj(slice.atom_id(), ".")?;
    //    let atom: AtomBody = serde_json::from_str(&atom_json)?;
    //    Ok(atom.atom().to_owned())
    //}

    /// Prepopulated redis conn for graphql testing. Flushes all keys upon
    /// successful connection.
    pub fn example_connectionp(redis_path: &str) -> Result<Self> {
        let db = Self::new(redis_path)?;
        // db.flush_db(); // this breaks so much shit due to parallel test
        //execution.

        let v0 = "v0 content";
        let vid0 = db.set_atomic_chunk(v0)?;
        let bid0 = db.new_node(&vid0)?;

        let v1 = "v1 content";
        let vid1 = db.set_atomic_chunk(v1)?;
        let bid1 = db.new_node(&vid1)?;

        let v2 = "v2 content";
        let vid2 = db.set_atomic_chunk(v2)?;
        let bid2 = db.new_node(&vid2)?;

        db.link(&bid1.id(), &bid0.id())?;
        db.link(&bid2.id(), &bid1.id())?;
        db.link(&bid0.id(), &bid2.id())?;

        Ok(db)
    }

    /// Prepopulated redis conn for graphql testing.
    pub fn example_connection() -> Result<Self> {
        Self::example_connectionp("redis://redis-net-alias")
    }
}
