// edge.rs
//! Provides full and relative edges.
//!
//! NB: Since most edges used internally are "relative" edges, i.e., edges
//! which omit the containing node id, assume that edge type are relative (and
//! directed?) unless explicitly specified.

// TODO (perhaps?) add struct `Adjacencies`, with adj. list manipulation API
// including, but not limited to the following:
//
// * filter by edge type, e.g., get all `DirRelEdgSe::PointsTo` variants that
//   implement `Comments` (a hypothetical marker trait).

use std::any::Any;
use std::iter::{FromIterator, IntoIterator};

use serde;
use serde_derive;

use vmap::atom::{self, Selection};
use vmap::node;

pub trait AsAny {
    fn as_any(&self) -> Box<Any>;
}

// # Full Edges

/// Marks directed (full) edge types.
pub trait GenericDirEdge {}

/// Complement of `DirRelEdge`, `DirEdge` denotes a generic directed relationship
/// between two selectable/indexable nodes, `src` and `tgt`. `src` points to
/// `tgt` and `tgt` is pointed to by `src`.
pub struct DirEdge<N> {
    /// Node from which the edge (`self`) stems; that is, the commentary.
    src: N,
    /// Node to which `self` points--i.e, the commented upon.
    tgt: N,
    /// Edge weight comprising arbitrary data necessary to determine of the
    /// edge should be traversed or not.
    ///
    /// E.g., suppose that `(Whole, Partial(3, 8)` is the weight of some
    /// of comment edge between two nodes `m`, and `n`; then all of `m`
    /// comments on the `Sel::Partial[3..8]` of `n`.
    ///
    /// tldr; read `weight` as `(src_sel, tgt_sel)`.
    ///
    /// See `Sel` for more detailed selection documentation.
    weight: (Sel, Sel),
}

// # Relative Edges

/// Marks directed relative edge types.
pub trait GenericDirRelEdge<N, W> {
    fn variant(&self) -> EdgeType;

    /// A relative edge contains only one node, which this function returns in
    /// addition to the edge's direction, relative to `node: N`.
    fn node(&self) -> Direction<N>;
    /// Returns the edge weight.
    fn weight(&self) -> W;
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub enum Direction<N> {
    To(N),
    From(N),
}

impl<N> Direction<N> {
    fn into_unwrapped(self) -> N {
        match self {
            Direction::To(n) => n,
            Direction::From(n) => n
        }
    }
    fn as_ref(&self) -> &N {
        match self {
            Direction::To(n) => &n,
            Direction::From(n) => &n,
        }
    }

    fn to(n: N) -> Self {
        Direction::To(n)
    }

    fn from(n: N) -> Self {
        Direction::From(n)
    }
}

/// There shoud exist one variant, or `Other(variant_label)` for each  _concrete_ edge type.
pub enum EdgeType {
    Weighted,
    Select,
    // provids edge type extensibility
    Other(String),
}

#[derive(Clone, Debug, PartialEq, Eq)]
pub enum Edge<N, W> {
    /// Do not use this variant.
    Weighted(WeightedRelEdge<N, W>),
    Select(Select<N>),
    Other(String), // json obj for runtine edge "type" generation
}

#[derive(Clone, Debug, PartialEq, Eq)]
pub enum Weight {
    Nothing,
    Select(Sel, Sel),
    Other(String), // for json runtime weight variant creation
}

impl Weight {
    fn to_select_weight<N, W>(&self) -> Option<(Sel, Sel)> {
        match self {
            Weight::Select(s, e) => Some((s.to_owned(), e.to_owned())),
            _                    => None
        }
    }
}

impl From<(Sel, Sel)> for Weight {
    fn from((s, e): (Sel, Sel)) -> Self {
        Weight::Select(s, e)
    }
}

/// `DirRelEdge` is a directed relative edge, meant to signify a generic directed relationship
/// between two selectable/indexable nodes.
///
///
/// The two `Sel`s specify the source node and target node selections
/// respectively.
///
/// See `Comments` for full edge.
///
/// NOTE `weight` is stored in each node referenced by the edge.
/// A triple of, in order, target node (id), source node selection, and
/// target node selection.
#[derive(Clone, Debug, PartialEq, Eq)]
pub struct Select<N> {
    /// The selection of the focused, or current node.
   focus_sel: Sel,
   /// The selection and id of the node attached to the end of the edge the
   /// instance of `Select` describes.
   tgt: (Sel, Direction<N>)
}

impl<N: Clone> Select<N> {
    pub fn new(focus_sel: Sel, tgt: (Sel, Direction<N>)) -> Self {
        Select {
            focus_sel,
            tgt
        }
    }

    pub fn focus_sel(&self) -> Sel {
        self.focus_sel.to_owned()
    }
    
    pub fn tgt_node(&self) -> Direction<N> {
         self.tgt.1.to_owned()
     }

    fn tgt(&self) -> (Sel, Direction<N>) {
        self.tgt.to_owned()
    }

    fn tgt_sel(&self) -> Sel {
        self.tgt.0.to_owned()
    }
} 


#[derive(Clone, Debug, PartialEq, Eq)]
pub struct WeightedRelEdge<N, W>(Direction<N>, W);

impl<N, W> WeightedRelEdge<N, W> {
    pub fn new(n: Direction<N>, w: W) -> Self {
        WeightedRelEdge(n, w)
    }
}

impl<N, W> GenericDirRelEdge<N, W> for WeightedRelEdge<N, W>
where
    N: Clone,
    W: Clone,
{
    fn variant(&self) -> EdgeType {
        EdgeType::Weighted
    }

    fn node(&self) -> Direction<N> {
        self.0.to_owned()
    }

    fn weight(&self) -> W {
        self.1.to_owned()
    }
}

/// See `Select`.
#[derive(Debug, Clone, PartialEq, Eq)]
// TODO remove dep on `WeightedRelEdge` as its consistinig of directions other
// that `Direction::To` is unrepresentative.
pub struct PointsTo<N>{
    frm_sel: Sel, 
    tgt: (Sel, N)
}

impl<N: Clone> PointsTo<N> {
    pub fn new(frm_sel: Sel, tgt: (Sel, N)) -> Self {
        PointsTo { frm_sel, tgt }
    }

    /// Returns the `node: N` towards which this struct represenst an edge.
    pub fn node(&self) -> N {
        self.tgt.1.to_owned()
    }

    pub fn weight(&self) -> (Sel, Sel) {
        (self.frm_sel.to_owned(), self.tgt.0.to_owned())
    }

    pub fn try_from(s: &Select<N>) -> Option<Self> {

        let fs = s.focus_sel();
        let t= s.tgt_sel();
        match s.tgt_node() {
            Direction::To(n) => Some(PointsTo::new(fs, (t, n))),
            _ => None,
        }
    }
}


/// For filtering and homogenizing adjancency lists of the type
/// `Vec<Box<GenericDirRelEdge>>`.
#[derive(Debug, Clone, PartialEq, Eq)]
pub struct IsPointedToBy<N> {
    focus_sel: Sel,
    tgt: (Sel, N)
}

impl<N: Clone> IsPointedToBy<N> {

    pub fn new(focus_sel: Sel, tgt: (Sel, N)) -> Self {
        IsPointedToBy { focus_sel, tgt }
    }

    pub fn tgt_node(&self) -> Direction<N> {
        let n = self.tgt.1.to_owned();
        Direction::from(n)
    }

    fn tgt(&self) -> (Sel, N) {
        self.tgt.to_owned()
    }
    
    pub fn try_from(s: &Select<N>) -> Option<Self> {
        let focus = s.focus_sel();
        let t = s.tgt_sel();
        match s.tgt_node() {
            Direction::From(n) => Some(IsPointedToBy::new(focus, (t, n))),
            _ => None,
        }
    }
}


/// Denotes how much, if any, of a node's content is referenced by an edge.
/// E.g., suppose a  `Comments` edge, `e`, connects some commentary node, `c`, and some
/// slice of a target node, `n`. Then edge `e` must not only indicate between
/// which nodes it spans, and in what direction it spans, but also which subset
/// of `c` comments upon which subset of `n`. Since all of `c` comments upon a
/// selection of `n`, the edge would look like:
// ```
// use Sel::*;
//
// Comment {
//     // comment node
//     src: c,
//     // commented upon node
//     tgt: n,
//     // edge weight tuple of `(src_sel, tgt_sel)`
//     weight: (All, Partial(Selection(...))),
// }
// ```
#[derive(Clone, Debug, PartialEq, Eq, Serialize, Deserialize)]
pub enum Sel {
    /// All of the associated node is referenced by this selection variant.
    All,
    /// A contigous slice of the assoc'd node is referenced by this variant.
    /// E.g., to refer to a (contiguos) slice of a node from index `s` to
    /// index`e`, the `Sel would be `Partial(s, e)`, where `s` is inclusive,
    /// and `e` is exclusive.
    ///
    /// NB: nodes are supposed to refer to a selectable/indexable medium.
    /// A.t.m., the only supported type is `String`; this, however, will be
    /// remedied.
    Partial(usize, usize),
    /// A nominal, empty selection of assoc'd node. Intended for use when
    /// neither `All` nor `Partial(..)` suffice to capture the intended
    /// referent, so that such a relation is not wrapped by `Partial(..)`, or
    /// `All` due only to the lack of a more descriptive selection. If
    /// `Nothing` is used frequently, a custom edge type may be more
    /// apropriate.
    Nothing,
}

impl Sel {
    fn select<I>(&self, i: I) -> Option<I>
    where
        I: FromIterator<<I as IntoIterator>::Item> + IntoIterator,
    {
        match self {
            Sel::Partial(s, e) => {
                let ret: I = i.into_iter().skip(*s).take(*e - *s).collect();
                Some(ret)
            }
            Sel::All => Some(i),
            Sel::Nothing => None,
        }
    }
}

/// Marker trait for selectable content types. Implement for edge types whose
/// weights may include `Sel`.
trait Selectable<I>
where
    I: FromIterator<<I as IntoIterator>::Item> + IntoIterator,
{
    /// Implement for `Node` where `I = AtomBody`
    ///
    /// Analogue of `into_iter`.
    fn into_selectable(&self) -> I;

    /// Analogue of `iter`.
    fn selectable(&self) -> I;

    /// Analogue of `iter_mut`.
    fn selectable_mut(&self) -> I;

    fn select(&self, sel: Sel) -> Option<I> {
        let i = self.into_selectable();
        match sel {
            Sel::Partial(s, e) => {
                let ret: I = i.into_iter().skip(s).take(e - s).collect();
                Some(ret)
            }
            Sel::All => Some(i),
            Sel::Nothing => None,
        }
    }
}
