//! Actix web juniper example
//!
//! A simple example integrating juniper in actix-web
extern crate liberos;

fn main() {
    println!("About to start server..");
    liberos::serve_graphql();
}
