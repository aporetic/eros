#![allow(unused)]
#[macro_use]
extern crate juniper;
#[macro_use]
extern crate serde_derive;
#[macro_use]
extern crate error_chain;
#[macro_use]
extern crate serde_json;
extern crate actix;
extern crate actix_web;
extern crate base64;
extern crate crypto;
extern crate env_logger;
extern crate futures;
extern crate r2d2;
extern crate r2d2_redis;
extern crate redis;
extern crate serde;

use juniper::{FieldError, FieldResult, Value};
use std::cell::RefCell;
use std::collections::hash_map::{DefaultHasher, HashMap};
use std::hash::{Hash, Hasher};

pub mod errors;
#[macro_use]
mod macros;
pub mod graphql;
#[cfg(test)]
mod tests;
pub mod vmap;

use errors::*;
use graphql::*;
use vmap::*;

/// Hash the given `Hashable` value with the provided `Hasher`.
pub fn hash_with(x: &impl Hash, hasher: &mut impl Hasher) -> u64 {
    x.hash(hasher);
    hasher.finish()
}

/// Hash a `Hashable` with the `DefaultHasher`.
pub fn hash_default(x: &impl Hash) -> u64 {
    let mut h = DefaultHasher::new();
    hash_with(x, &mut h)
}

use actix::prelude::*;
use actix_web::middleware::cors::Cors;
use actix_web::{http, middleware, server, App, AsyncResponder, Error, FutureResponse, HttpRequest,
                HttpResponse, Json, State};
use futures::future::Future;
use juniper::http::GraphQLRequest;
use juniper::http::graphiql::graphiql_source;

struct AppState {
    executor: Addr<Syn, GraphQLExecutor>,
}

#[derive(Serialize, Deserialize)]
pub struct GraphQLData(GraphQLRequest);

impl Message for GraphQLData {
    type Result = ::std::result::Result<String, Error>;
}

/// Allows manual request dispatch via `GraphQLRequest::execute()`
pub struct GraphQLExecutor {
    schema: std::sync::Arc<Schema>,
    context: DB, // r2d2 redis conn pool?
}

impl GraphQLExecutor {
    fn new(schema: std::sync::Arc<Schema>) -> GraphQLExecutor {
        GraphQLExecutor {
            schema: schema,
            context: DB::example_connection().unwrap(), // TODO fix
        }
    }
}

impl Actor for GraphQLExecutor {
    type Context = SyncContext<Self>;
}

impl Handler<GraphQLData> for GraphQLExecutor {
    type Result = ::std::result::Result<String, Error>;

    /// Dispatch request (`GraphQLData`) with schema stored in
    /// `AppState`
    fn handle(&mut self, msg: GraphQLData, _: &mut Self::Context) -> Self::Result {
        let res = msg.0.execute(&self.schema, &self.context);
        let res_text = serde_json::to_string(&res)?;
        Ok(res_text)
    }
}

fn graphiql(_req: HttpRequest<AppState>) -> ::std::result::Result<HttpResponse, Error> {
    let html = graphiql_source("http://0.0.0.0:8080/graphql");
    Ok(HttpResponse::Ok()
        .content_type("text/html; charset=utf-8")
        .body(html))
}

fn graphql((st, data): (State<AppState>, Json<GraphQLData>)) -> FutureResponse<HttpResponse> {
    st.executor
        .send(data.0)
        .from_err()
        .and_then(|res| match res {
            Ok(user) => Ok(HttpResponse::Ok()
                .content_type("application/json")
                .body(user)),
            Err(_) => Ok(HttpResponse::InternalServerError().into()),
        })
        .responder()
}

//fn cors(app: App) -> App {
//    Cors::build()
//        .allowed_origin("http://0.0.0.0/graphql")
//        .allowed_methods(vec!["GET", "POST"])
//        .finish()
//}

pub fn serve_graphql() {
    ::std::env::set_var("RUST_LOG", "actix_web=error");
    env_logger::init();
    let sys = actix::System::new("juniper-example");

    let schema = std::sync::Arc::new(create_schema());
    let addr = SyncArbiter::start(3, move || GraphQLExecutor::new(schema.clone()));

    // Start http server
    server::new(move || {
        App::with_state(AppState{executor: addr.clone()})
            // enable logger
            .middleware(middleware::Logger::default())
            .configure(|app| {
                Cors::for_app(app)
                    .allowed_origin("http://127.0.0.1:8080")
                    .allowed_methods(vec!["GET", "POST", "OPTIONS"])
                    .resource("/graphql", |r| r.method(http::Method::POST).with(graphql))
                    .register()
            })
            .resource("/graphiql", |r| r.method(http::Method::GET).h(graphiql))
    }).bind("0.0.0.0:8080")
        .unwrap()
        .start();

    println!("Started http server: 0.0.0.0:8080");
    let _ = sys.run();
}
