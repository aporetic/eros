use super::juniper::{execute, Variables};
use super::*;
use std::ops::Deref;

// %s;redis-net-alias;127.0.0.1:6379;g
// %s;127.0.0.1:6379;redis-net-alias;g
#[test]
fn select_() {
    let db = DB::example_connectionp("redis://redis-net-alias").unwrap();
    let schema = create_schema();
    let hello = r#"
    mutation {
        createAtomFromStr(s: "hello world") {
                deref
            }
    }
    "#;

    let _ = execute(hello, None, &schema, &Variables::new(), &db).unwrap();

    let doc = r#"
mutation {
  viewSelection(atomId: "body_idYjk0ZDI3Yjk5MzRkM2UwOGE1MmU1MmQ3ZGE3ZGFiZmFjNDg0ZWZlMzdhNTM4MGVlOTA4OGY3YWNlMmVmY2RlOQ==", s: 3, e: 8)
	{ deref }
} "#;
    let exp = r#"{"viewSelection":{"deref": "lo wo"}}"#;
    cmp_query_and_res(&db, &schema, doc, exp, vec![vec!["viewSelection", "deref"]]).unwrap();
}

#[test]
fn create_atom() {
    //let db = DB::new("redis://127.0.0.1:6379").unwrap();
    let db = DB::example_connectionp("redis://redis-net-alias").unwrap();
    let schema = create_schema();
    let doc = r#"
    mutation {
        createAtomFromStr(s: "hello world") {
                id
                deref
            }
    }
    "#;
    let act = execute(doc, None, &schema, &Variables::new(), &db);

    let exp: ::std::result::Result<(Value, Vec<juniper::ExecutionError>), juniper::GraphQLError<'_>>
        = Ok((Value::object(
                vec![
                    ("createAtomFromStr", Value::object(vec![("id", Value::string("body_idYjk0ZDI3Yjk5MzRkM2UwOGE1MmU1MmQ3ZGE3ZGFiZmFjNDg0ZWZlMzdhNTM4MGVlOTA4OGY3YWNlMmVmY2RlOQ==")), ("deref", Value::string("hello world"))].into_iter().collect()))
                ].into_iter().collect()),
            vec![]
            ));
    assert_eq!(act, exp);
    let create_node = r#"
    mutation {
        createNode(atomId: "body_idYjk0ZDI3Yjk5MzRkM2UwOGE1MmU1MmQ3ZGE3ZGFiZmFjNDg0ZWZlMzdhNTM4MGVlOTA4OGY3YWNlMmVmY2RlOQ==")
        {
            deref
        }
    }
    "#;
    let act = execute(create_node, None, &schema, &Variables::new(), &db);
    let exp: ::std::result::Result<
        (Value, Vec<juniper::ExecutionError>),
        juniper::GraphQLError<'_>,
    > = Ok((
        Value::object(
            vec![
                (
                    "createNode",
                    Value::object(
                        vec![("deref", Value::string("hello world"))]
                            .into_iter()
                            .collect(),
                    ),
                ),
            ].into_iter()
                .collect(),
        ),
        vec![],
    ));

    println!("actual: {:#?}", &act);
    println!("expected: {:#?}", &exp);
    assert_eq!(act, exp);

    let create_atom1 = r#"
    mutation {
        createAtomFromStr(s: "this is the end") { deref }
    }
    "#;
    let the_end = execute(create_atom1, None, &schema, &Variables::new(), &db);
    println!("\nthe_end: {:?}\n", the_end);
    let keys: Vec<String> = db.keys("*").unwrap();
    //println!("{:#?}", keys);
    for k in keys.iter() {
        let v: String = db.getj(k, ".").unwrap();
        println!("k = {}: v = {}", k, v);
    }
    println!("");
    let create_node1 = r#"
    mutation {
    createNode(atomId: "body_idZjM0ZDM1NmZhMjg5YTAzYWU4N2I2ZTI5MjQxYTIyZjAwYjc3MmIyOWE5OWVmZWNiZDJlZjhkNDBjY2YzMjI2Mw==")
        {
            deref
        }
    }
    "#;

    let act = execute(create_node1, None, &schema, &Variables::new(), &db);
    let exp: ::std::result::Result<
        (Value, Vec<juniper::ExecutionError>),
        juniper::GraphQLError<'_>,
    > = Ok((
        Value::object(
            vec![
                (
                    "createNode",
                    Value::object(
                        vec![("deref", Value::string("this is the end"))]
                            .into_iter()
                            .collect(),
                    ),
                ),
            ].into_iter()
                .collect(),
        ),
        vec![],
    ));
    assert_eq!(act, exp);

    let link = r#"
    mutation {
        link(n0: "node_idNjYzMDMyOTBkOTA1MGM1MDk0MDA1MDljOTJjZDJmNTlmYjBkNjRkYzdhZDUwYTMyMGIzMzM2ZTYwYzllN2EyOA==",
             n1: "node_idMjE5YmRiNzY2ZmUzNzg0YWM0YWE0OTcxYzhiYjExOGVmZDI5ZWZhZGFiMTk1OTllNDFhZGM0MzExODRlMTNlZg==") {
            deref
        }
    }"#;
    let act = execute(link, None, &schema, &Variables::new(), &db);
    let exp: ::std::result::Result<
        (Value, Vec<juniper::ExecutionError>),
        juniper::GraphQLError<'_>,
    > = Ok((
        Value::object(
            vec![
                (
                    "link",
                    Value::list(
                        vec![
                            Value::Object(
                                vec![("deref".to_owned(), Value::string("this is the end"))]
                                    .into_iter()
                                    .collect(),
                            ),
                            Value::Object(
                                vec![("deref".to_owned(), Value::string("hello world"))]
                                    .into_iter()
                                    .collect(),
                            ),
                        ].into_iter()
                            .collect(),
                    ),
                ),
            ].into_iter()
                .collect(),
        ),
        vec![],
    ));
    //println!("act_val: {:?}", &act);
    assert_eq!(act, exp);

    // next test here
    // `link` seems broken.
    // the below should reveal that "hello.." <-> "this is ...", but instead
    // shows "hell.."->["hel..", "hel.."], "this.."-> [].
    //let broken_link = r#"
    //mutation {
    //    link(n0: "YzEyMDI5Njg4OWIzYzZlZjdlYjhkMjc0NmU1MjVlZWUyMmFmNjA1ZjNkMjk2MTk3MGYzM2FkMjljYTdkN2JhMg==", n1: "YTE1OWMwYmU3NTNmNjAzZTc2YWNjNTIxYWQyNzY2ZmMxMWMzMjFlMDFkYWE3NTZlZGNmZjVlOTYwY2M5YjNiNA==") {
    //        deref
    //        pointsTo {
    //            deref
    //        }
    //        isPointedToBy {
    //            deref
    //        }

    //    }
    //}"#;
    //let act = execute(broken_link, None, &schema, &Variables::new(), &db);
    //let exp: ::std::result::Result<
    //    (Value, Vec<juniper::ExecutionError>),
    //    juniper::GraphQLError<'_>,
    //> = Ok((
    //    Value::object(
    //        vec![
    //            (
    //                "link",
    //                Value::list(
    //                    vec![
    //                        Value::Object(
    //                            vec![
    //                                ("deref".to_owned(), Value::string("this is the end")),
    //                                (
    //                                    "pointsTo".to_owned(),
    //                                    Value::list(vec![
    //                                        Value::object(
    //                                            vec![("deref", Value::string("hello world"))]
    //                                                .into_iter()
    //                                                .collect(),
    //                                        ),
    //                                    ]),
    //                                ),
    //                                ("isPointedToBy".to_owned(), Value::list(vec![])),
    //                            ].into_iter()
    //                                .collect(),
    //                        ),
    //                        Value::Object(
    //                            vec![
    //                                ("deref".to_owned(), Value::string("hello world")),
    //                                ("pointsTo".to_owned(), Value::list(vec![])),
    //                                (
    //                                    "isPointedToBy".to_owned(),
    //                                    Value::list(vec![
    //                                        Value::object(
    //                                            vec![("deref", Value::string("this is the end"))]
    //                                                .into_iter()
    //                                                .collect(),
    //                                        ),
    //                                    ]),
    //                                ),
    //                            ].into_iter()
    //                                .collect(),
    //                        ),
    //                    ].into_iter()
    //                        .collect(),
    //                ),
    //            ),
    //        ].into_iter()
    //            .collect(),
    //    ),
    //    vec![],
    //));

    //println!("actual: {:#?}", &act);
    //println!("expected: {:#?}", &exp);
    //assert_eq!(act, exp);
}
/// Compares the `serde_json::Value`s to which any path must point--invocations with those that point
/// to elsewhere will return `Err`. Since this is meant for testing,
/// `assert_eq!` will be used instead of folding AND (`&&`) over a list of
/// `bool`.
fn cmp_query_and_res(
    db: &DB,
    schema: &Schema,
    query: &str,    // graphql query
    exp_resp: &str, // expected response in json
    paths: Vec<Vec<&str>>,
) -> Result<bool> {
    let res: Result<Value> = match execute(query, None, schema, &Variables::new(), db) {
        Ok(tup) => Ok(tup.0), // extract `Value`
        Err(e) => Err(format!("cmp_query_and_res: GraphQLError: {:?}", e).into()),
    };
    let act = res?;
    let json = serde_json::to_string(&act)?;
    let act_val: serde_json::Value = serde_json::from_str(&json)?;
    let exp_val: serde_json::Value = serde_json::from_str(&exp_resp)?;
    for ps in paths.iter() {
        let mut it = ps.iter();
        let mut act = &act_val;
        let mut exp = &exp_val;
        while let Some(path) = it.next() {
            act = &act[path];
            exp = &exp[path];
        }
        assert_eq!(*act, *exp);
    }
    Ok(true)
}

/// Like the above, but sorts arrays before comparison.
fn cmp_array(
    db: &DB,
    schema: &Schema,
    query: &str,           // graphql query
    exp_resp: &str,        // expected response in json
    paths: Vec<Vec<&str>>, // paths into to json array
    element_path: &str,
) -> Result<bool> {
    let res: Result<Value> = match execute(query, None, schema, &Variables::new(), db) {
        Ok(tup) => Ok(tup.0), // extract `Value`
        Err(e) => Err(format!("cmp_query_and_res: GraphQLError: {:?}", e).into()),
    };
    let act = res?;
    let json = serde_json::to_string(&act)?;
    let act_val: serde_json::Value = serde_json::from_str(&json)?;
    let exp_val: serde_json::Value = serde_json::from_str(&exp_resp)?;
    for ps in paths.iter() {
        let mut it = ps.iter();
        let mut act = &act_val;
        let mut exp = &exp_val;
        while let Some(path) = it.next() {
            act = &act[path];
            exp = &exp[path];
        }
        let mut act_arr: Vec<serde_json::Value> = (*act).as_array().map(|v| v.to_vec()).unwrap();
        println!("act_arr: {:#?}", &act_arr);
        let mut al_opt: Option<Vec<&str>> = act_arr
            .iter_mut()
            .map(|obj| obj[element_path].as_str())
            .collect();
        println!("al_opt: {:?}", &al_opt);
        let mut act_fin = al_opt.unwrap();
        act_fin.sort();

        let mut exp_arr: Vec<serde_json::Value> = (*exp).as_array().map(|v| v.to_vec()).unwrap();
        let mut exp_opt: Option<Vec<&str>> = exp_arr
            .iter_mut()
            .map(|obj| obj[element_path].as_str())
            .collect();
        println!("exp_opt: {:?}", &exp_opt);
        let mut exp_fin = exp_opt.unwrap();
        exp_fin.sort();
        assert_eq!(act_fin, exp_fin);
    }
    Ok(true)
}

#[test]
fn it_works() {}

//fn get_variant(val: serde_json::Value) -> String {
//    match val {
//        Value::Null => "Null",
//        Value::Int => "Int",
//        Value::
//    }
//}

fn nodes_() {
    let db = DB::example_connectionp("redis://redis-net-alias").unwrap();
    let schema = create_schema();
    let pconn = db.get_conn().unwrap();
    let conn = pconn.deref();
    let _: usize = redis::cmd("JSON.DEL")
                         .arg("body_idYjk0ZDI3Yjk5MzRkM2UwOGE1MmU1MmQ3ZGE3ZGFiZmFjNDg0ZWZlMzdhNTM4MGVlOTA4OGY3YWNlMmVmY2RlOQ==")
                         .arg(".")
                         .query(conn).unwrap();

    let doc = r#"{ nodes { deref }}"#;
    let exp_resp = r#"
{
    "nodes": [
      {
        "deref": "v0 content"
      },
      {
        "deref": "v2 content"
      },
      {
        "deref": "v1 content"
      }
    ]
  }
    "#;
    let paths = vec![vec!["nodes"]];
    let element_path = "deref";
    cmp_array(&db, &schema, doc, exp_resp, paths, element_path).unwrap();

    let doc = r#"{ nodes { pointsTo { deref }}}"#;
    let exp_resp = r#"
{
    "nodes": [
      {
        "pointsTo": [
          {
            "deref": "v2 content"
          }
        ]
      },
      {
        "pointsTo": [
          {
            "deref": "v1 content"
          }
        ]
      },
      {
        "pointsTo": [
          {
            "deref": "v0 content"
          }
        ]
      }
    ]
  }
    "#;
    let paths = vec![vec!["nodes"]];
    let element_path = "";
    //cmp_array(&db, &schema, doc, exp_resp, paths, element_path).unwrap();
    // TODO make it easier to compare nested json structures
}

fn atoms_() {
    //let db = DB::new("redis://127.0.0.1:6379").unwrap();
    let db = DB::example_connectionp("redis://redis-net-alias").unwrap();
    let schema = create_schema();
    let doc = r#"
    mutation {
        createAtomFromStr(s: "hello world") {
                deref
            }
    }
    "#;

    let exp = r#"{"createAtomFromStr":{"deref":"hello world"}}"#;
    // get `Value`.
    let act: Value = execute(doc, None, &schema, &Variables::new(), &db)
        .unwrap()
        .0;
    let json = serde_json::to_string(&act).unwrap();
    let act_val: serde_json::Value = serde_json::from_str(&json).unwrap();
    let exp_val: serde_json::Value = serde_json::from_str(&exp).unwrap();
    //assert_eq!(act_val["createAtomFromStr"]["deref"], exp_val["createAtomFromStr"]["deref"]);
    println!("{}", act_val);
    cmp_query_and_res(
        &db,
        &schema,
        doc,
        exp,
        vec![vec!["createAtomFromStr", "deref"]],
    ).unwrap();

    let the_end = r#"
    mutation {
        createAtomFromStr(s: "this is the end") {
                deref
            }
    }
    "#;
    let _ = execute(the_end, None, &schema, &Variables::new(), &db).unwrap();

    let more = r#"
    mutation {
        createAtomFromStr(s: "i want more!") {
                deref
            }
    }
    "#;

    let _ = execute(the_end, None, &schema, &Variables::new(), &db).unwrap();
    // complete test case w/ `array_cmp`
    let doc = r#"{
        atoms {
            deref
        }
    }"#;
    let mut derefs: Vec<&str> = vec![
        "hello world",
        "this is the end",
        "v2 content",
        "v1 content",
        "v0 content",
    ];
    let exp_json = r#"
    {
        "atoms": [
            { "deref": "hello world" },
            { "deref": "this is the end" },
            { "deref": "v0 content" },
            { "deref": "v1 content" },
            { "deref": "v2 content" }
        ]
    } "#;
    cmp_array(&db, &schema, doc, exp_json, vec![vec!["atoms"]], "deref").unwrap();
    // end of test case
}
// TODO rewrite all query tests with json `juniper::Value`s

// worthless, bc the order of `nodes` is _not_ preserved.
fn cycle_deref() {
    //let db = DB::example_connectionp("redis://127.0.0.1:6379").unwrap();
    let db = DB::example_connectionp("redis://redis-net-alias").unwrap();
    let schema = create_schema();
    let doc = r#"
        {
            nodes {
                deref
            }
        }"#;

    let act = execute(doc, None, &schema, &Variables::new(), &db);
    let exp: ::std::result::Result<
        (Value, Vec<juniper::ExecutionError>),
        juniper::GraphQLError<'_>,
    > = Ok((
        Value::object(
            vec![
                (
                    "nodes",
                    Value::list(
                        vec![
                            Value::Object(
                                vec![("deref".to_owned(), Value::string("v2 content"))]
                                    .into_iter()
                                    .collect(),
                            ),
                            Value::Object(
                                vec![("deref".to_owned(), Value::string("v1 content"))]
                                    .into_iter()
                                    .collect(),
                            ),
                            Value::Object(
                                vec![("deref".to_owned(), Value::string("v0 content"))]
                                    .into_iter()
                                    .collect(),
                            ),
                        ].into_iter()
                            .collect(),
                    ),
                ),
            ].into_iter()
                .collect(),
        ),
        vec![],
    ));
    println!("actual: {:#?}", &act);
    println!("expected: {:#?}", &exp);
    assert_eq!(act, exp);
}
