// macros.rs
#![feature(macro_rules)]

macro_rules! invalid_type_error {
    ($v:expr, $det:expr) => {{
        fail!((
            redis::ErrorKind::TypeError,
            "Response was of incompatible type",
            format!("{:?} (response was {:?})", $det, $v)
        ));
    }};
}

/// Returns closure which will prepend the given `pre: &str` to the `Display`
/// string of `err`.
///
/// Use inside `juniper` field when converting foreign errors to `FieldError`,
/// which usage should prefix a message indicating the actual source of the
/// error, as opposed to its reading as some sort of wacky, inapt `FieldError`.
///
/// # Examples
/// ```
/// let res: result::Result<String, Error> = Ok("hello world".to_owned();
/// let mapped = res.map_err(to_field_error!("prepended error description"));
/// ```
macro_rules! to_field_error {
    ($pre:expr) => {{
        |ref err| FieldError::new(format!("{}: {}", $pre, err), Value::null())
    }};
}

macro_rules! fail {
    ($expr:expr) => {
        return Err(::std::convert::From::from($expr));
    };
}
