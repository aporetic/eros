## README

## Quickstart

### Build rejson (redis + module) image

* Builds the official redis image, and installs the reJSON module.

* This step is necessary for both execution methods presented below.

```bash
git clone https://gitlab.com/aporetic/reJSON.git
cd reJSON
docker build . -t rejson
```
or, after installing `docker-compose`
```bash
make test
```

### Run containers with `docker-compose`

#### Run

```bash
docker-compose up
# ... container runs ...
docker-compose down
```

#### Test
```bash
make test
```

#### Check (i.e., `cargo check`)
```bash
make check
```

## "Manual" build -- i.e., without `docker-compose`
### rust binary

NB: for most use-cases `docker-compose up` is preferable.

```bash
docker build . -t rusty-img
```


### Manually run container via `docker` 

For those avoiding `docker-compose`:

```bash
docker run \
    --rm \
    -e USER='aporia' \
    - v $PWD:/usr/src/app # mount host's working dir
    rusty-img \
    cargo init --bin --name rusty-app
```



### Development workflow

run redis--should only have to be run once, at the start of each dev session

This will run both the rejson and rust binary containers. When the binary
process completes execution and exits quickly, the below should suffice. For
servers and other long-running processes, `docker-compose restart bin` may
work (DISCLAIMER: totally untested).


```bash
docker-compose up
```

run rust binary
```bash
docker-compose run --rm bin
```


