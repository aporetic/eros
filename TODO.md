TODO

*  (priority: MED) add gql field
    `nodes_and_edges(focal_node: NodeId, comment_depth: usize, commented_depth: usize)
         -> Result<(Vec<VisNode>, Vec<VisEdge>)
   where a `VisNode` is a p much an Atom (i.e., a `Node` w/o decentralized
   edge info), and a `VisEdge` is a `(VisNodeId, VisNodeId)`.

   NB: consider whether this graph is directed. should there exist a `VisEdge` for
   each direction, (e.g., (n0, n1), AND (n1, n0) (directed), or just (n0pL, n1) (undirected)).


*  (priority: HIGH) Refactor with `Edge<T>`, i.e., typed edges so that `Edge::new(b0,
   b1, EdgeType::Selection::Whole)` (or some such) means that there is a `Node`
   at `b0: NodeId`, and at `b1: NodeId`, such that `b0` selects the whole of
   `b1`.

   *   impl Edge, RelEdge as both (if necessary) gql-tailored and native rust
       structures.

   *  replace graphql api with new `node :: NodeId -> [RelEdge]` based one

   Desired graphql API/functionality (root) for milestone #1: comment graph,
   where nodes have `String`, and edges are `Select`

       *  createNodeFromStr :: &str -> Node
       *  link :: NodeId -> Sel -> NodeId -> Sel -> Edge
       *  selects :: NodeId -> [PointsTo]  // imply direction (To), or return
       *  custom edge, i.e., `PointsTo`?
       *  isSelectedBy :: NodeId -> [IsPointedToBy] // 
       *  serializeToDot :: NodeId // focus node
                         -> i32 // depthy: all nodes _depth_ steps from the
                                // focus are fetched
                         -> ([Node],[Edge]) // these being of the mathematical
                         // variety. E.g., `([a, b], [(All, a, Partial, b), (Partial, b, All, b)]`,
                         // which reads: 
                                "There exist two nodes, a, and b, such that all
                                of a selects a part of b and a part of b
                                selects all of a."
            Q: should edges of nodes on the perimeter of the circle of
               radius=depth be completely ignored, or be replaced by stubs to
               indicate that _something_ is there, to distinguish between a
               node on the global perimiter and one on a local,
               perspective-bound perimiter?

       NOTE: forall gql fields f, f :: NodeId -> ... -> *, f has an analog g,
       such that g :: Node -> ... -> *.

   Q: add typed `Node`s as well? `AtomId`s ?
      * via `PhantomData`?
      * could allow graph filters based on node *and* edge type.
   A: **YES**
    
       i. A common mathematical model of a graph (directed or undirected) is a
          tuple (N, E), where N and E and vectors of nodes and edges
          respectively.  Within `eros` these will be called `Edges`, and nodes
          will be called `NodeId`s, used in this tupled format for two
          purposes: conversion to `graphviz` DOT format; and client-side
          rendering--the pattern is relatively clear: the tupled model is a
          standard graph representation, and as such, to and from which `eros
          needs to support conversion.  NB: `Edges` must, by definition,
          contain *all* information pertinent to the relationship denoted,
          comprised by a triple: (src_node_id, tgt_node_id, weight), where the
          weight is some arbitrary data associated with edge.

     ii. The internal graph representation chosen for `eros` uses adjacency
          lists. The adjacency list model was chosen for the fast lookup of
          adjacent nodes (O(v_len), where v_len is the number of nodes in the list)).

      There are several ways to implement edge storage under this model:

          * Store _full_ `Edge`s by reference (identifiers, not pointers), and
            store typed wrappers (enum, or trait -- see
            ~/lab-repos/enum-vs-trait ) to indicate edge type without fetching
            the full edge -- so that an adj. list can be filtered quickly by
            edge type.

            - increased mem footprint due to identifier, edeg storage
            + decreased mem footprint due to by reference storage of edges in
              nodes.
            + full edge reconstruction not required when converting to the 
              (N, E) model.
            - perf hit (speed) due to additonal degree of indirection
              introduced by storage of edges in db.
        
          * Store "partial", or "relative" edges by value in each node's adj.
            list. Such an edge would convey only information necessary to
            determine, without any further db requests, a) the type of the edge,
            and b) between which nodes the edge spans. A "relative" edge could
            also be interpreted as denoting one node's perspective of an edge,
            which view contains what's needed to determine if the attached node
            should be fetched or not in order to complete the current
            operation..

□ (priority: HIGH) Add support for multiple instances, or clusters, of redis
  dbs, and identifier namespacing. (Pre-req for "meta-graphs", i.e., graph
  merges, inter-graph edges, etc.

□ (priority: med) Integrate `artifact` design doc to:
      i. provide project status summary

     ii. require me to write better (more) specs

    iii. make salient untested modules, as well as dependencies upon such by well
         tested modules

□  (priority: low) Add `DB::get_schema`; see ./introQuery

□  (priority: low) Trim identifiers to 48-bits (hex), base64 encode (to 64-bits) and then parse
   to usize.

   * Why? Well, now except for the networked bits, `NodeId`s can be stored as
     a usize, except redis only stores byte[]. Maybe just shorten the
     `NodeId` to hex_n<=64, base64_n<=43+1, bin_n<=32 bytes, and then prepend
     "node_id<id>"?
   * To what will this apply? Definitely `NodeId`s as their being hashes
     adds no functionality. Whether to include `AtomId`s depends upon the
     requisite atom space--that is, upon how many different `Atoms` (body
     and slice) a single namespace may contain.
