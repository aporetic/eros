### Summary

Describe, as concisely as possible, the requested/proposed feature.


### Motivation and use case(s)

Explain why _you_ want this, and why/whether others might as well, i.e., list
some use cases.


### Example (recommended)

If possible, include below, in-line or in a linked repository, a self-contained
demonstration of the desired functionality in terms of:
    * the existing API (i.e., an example that compiles); or,
    * pseudo-code, or a black-box example.

### Implementation details (optional)

Provide, if possible, as many relevant details about possible implementations
of the requested feature. If you have already prototyped the feature, submit an
issue with only a link to the pull request containing the proposed feature.

Why, you might ask, should I still submit an issue when I'm ready to make a
pull request? Issues tagged `feature-request` document proposed/requested
features and sufficient documentation to remind a lost developer of _all_ past
feature requests, so as to conveniently avoid duplicating the work of
developers past.

/label ~feature-request
/cc @project-manager

