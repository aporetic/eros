### Summary

(Summarize bug concisely)


### Steps to reproduce

    i.  provide small, self-contained example of bug; or,
    ii. provide links to source code, and the build steps that led to 
        the (purported) bug.

For best results, provide both.


### What does the bug (seem) to do?

(Explain what went wrong)


### What was expected?

(Explain, and justify, the expected behavior)


### Relevant logs and/or screenshots

(Paste relevant logs in code blocks)


### Possible fixes

(Link to source code that might be responsible, and/or present possible causes
of the bug)

/label ~bug ~needs-investigation
/cc @project-manager

